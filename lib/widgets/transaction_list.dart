import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expenses/models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> _transactions;

  TransactionList(this._transactions);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 500,
        child: ListView.builder(
            itemCount: _transactions.length,
            itemBuilder: (context, index) {
              return Card(
                  child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    decoration: BoxDecoration(
                        border: Border.all(
                      color: Theme.of(context).primaryColor,
                      width: 2,
                    )),
                    padding: EdgeInsets.all(10),
                    width: 100,
                    child: Text(
                      '\$${_transactions[index].amount.toStringAsFixed(2)}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Theme.of(context).primaryColor),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _transactions[index].title,
                        style: TextStyle(
                            fontWeight: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .fontWeight,
                            color:
                                Theme.of(context).textTheme.bodyText1!.color),
                      ),
                      Text(
                        DateFormat('dd-MM-yyy' '    ' 'HH:mm')
                            .format(_transactions[index].date),
                        style: TextStyle(
                            color:
                                Theme.of(context).textTheme.bodyText2!.color),
                      )
                    ],
                  )
                ],
              ));
            }));
  }
}
